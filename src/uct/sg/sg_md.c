
#include "sg.h"

#ifndef SG_ENGINE_H
#define SG_ENGINE_H
#include <simgrid/engine.h>
#endif

#include <uct/base/uct_md.h>
#include <uct/api/v2/uct_v2.h>

static ucs_config_field_t uct_sg_md_config_table[] = {
    {NULL}
};

ucs_config_field_t uct_sg_sockcm_config_table[] = {
  {NULL}
};

static void uct_sg_md_close(uct_md_h md) {
    uct_sg_md_t *sg_md = ucs_derived_of(md, uct_sg_md_t);
    ucs_free(sg_md);
}

static ucs_status_t uct_sg_md_query(uct_md_h md, uct_md_attr_v2_t *attr) {
    return UCS_OK;
}

static uct_md_ops_t uct_sg_md_ops = {
    .close              = uct_sg_md_close,
    .query              = uct_sg_md_query,
    .mkey_pack          = ucs_empty_function_return_success,
    .mem_reg            = uct_md_dummy_mem_reg,
    .mem_dereg          = uct_md_dummy_mem_dereg,
    .detect_memory_type = ucs_empty_function_return_unsupported
};

static ucs_status_t
uct_sg_md_open(uct_component_t *component, const char *md_name,
               const uct_md_config_t *uct_md_config, uct_md_h *md_p) 
{
    printf("in uct_sg_md_open\n");

    uct_sg_md_t* sg_md;

    sg_md = ucs_malloc(sizeof(uct_sg_md_t), "uct_sg_md_t");
    sg_md->super.ops = &uct_sg_md_ops;
    sg_md->super.component = &uct_sg_component;

    *md_p = &sg_md->super;

    if (NULL == sg_md) {
        ucs_error("failed to allocate memory for uct_sg_md_t");
        return UCS_ERR_NO_MEMORY;
    }

    /* When your program starts, you have to first start a new simulation engine, as follows */
    int argc = 0;
    char** argv = NULL;
    simgrid_init(&argc, argv); // TODO should this be initated elsewhere?

    /* Then you should load a platform file, describing your simulated platform */
    simgrid_load_platform("../small_platform.xml");

    return UCS_OK;
}

static ucs_status_t 
uct_sg_cm_open(uct_component_t *component, uct_worker_h worker, 
               const uct_cm_config_t *uct_cm_config, uct_cm_h *cm_p) 
{
    printf("in uct_sg_cm_open\n");
    return UCS_OK;
}

static ucs_status_t uct_sg_md_rkey_unpack(uct_component_t *component,
                                          const void *rkey_buffer,
                                          uct_rkey_t *rkey_p, void **handle_p)
{
    /**
     * Pseudo stub function for the key unpacking
     * Need rkey == 0 due to work with same process to reuse uct_base_[put|get|atomic]*
     */
    *rkey_p   = 0;
    *handle_p = NULL;
    return UCS_OK;
}

uct_component_t uct_sg_component = {
    .query_md_resources = uct_md_query_single_md_resource,
    .md_open            = uct_sg_md_open,
    .cm_open            = uct_sg_cm_open, // UCS_CLASS_NEW_FUNC_NAME(uct_sg_sockcm_t),
    .rkey_unpack        = uct_sg_md_rkey_unpack,
    .rkey_ptr           = ucs_empty_function_return_unsupported,
    .rkey_release       = ucs_empty_function_return_success,
    .name               = UCT_SG_NAME,
    .md_config          = {
        .name           = "SG memory domain",
        .prefix         = "SG_",
        .table          = uct_sg_md_config_table,
        .size           = 0
        // .size           = sizeof(uct_sg_md_config_t)
    },
    .cm_config          = {
        .name           = "SG-SOCKCM connection manager",
        .prefix         = "SG_CM_",
        .table          = uct_sg_sockcm_config_table,
        .size           = 0
        // .size           = sizeof(uct_sg_sockcm_config_t),
     },
    .tl_list            = UCT_COMPONENT_TL_LIST_INITIALIZER(&uct_sg_component),
    .flags              = UCT_COMPONENT_FLAG_CM,
    .md_vfs_init        = (uct_component_md_vfs_init_func_t)ucs_empty_function
};