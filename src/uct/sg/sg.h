
#ifndef UCT_SG_H
#define UCT_SG_H

#include <uct/base/uct_md.h>
#include <uct/base/uct_iface.h>
#include <uct/base/uct_iov.inl>
#include <ucs/sys/sock.h>
#include <ucs/sys/string.h>
#include <ucs/datastruct/conn_match.h>
#include <ucs/datastruct/ptr_map.inl>
#include <ucs/algorithm/crc.h>
#include <ucs/sys/event_set.h>
#include <ucs/sys/iovec.h>

#define UCT_SG_NAME                          "sg"
#define UCT_SG_CONFIG_PREFIX                 "SG_"

static ucs_config_field_t uct_sg_iface_config_table[] = {
  {NULL}
};

typedef struct uct_sg_iface_config {
} uct_sg_iface_config_t;

typedef struct uct_sg_iface {
    uct_base_iface_t              super;             /* Parent class */
    int                           test;
} uct_sg_iface_t;

typedef struct uct_sg_ep {
    int test;
} uct_sg_ep_t;

typedef struct uct_sg_md {
    uct_md_t        super;
} uct_sg_md_t;

extern uct_component_t uct_sg_component;

ucs_status_t uct_sg_ep_am_short(uct_ep_h uct_ep, uint8_t am_id, uint64_t header,
                                const void *payload, unsigned length);

ucs_status_t uct_sg_ep_am_short_iov(uct_ep_h uct_ep, uint8_t am_id,
                                    const uct_iov_t *uct_iov, size_t uct_iov_cnt);

ssize_t uct_sg_ep_am_bcopy(uct_ep_h uct_ep, uint8_t am_id,
                           uct_pack_callback_t pack_cb, void *arg,
                           unsigned flags);

ucs_status_t uct_sg_ep_am_zcopy(uct_ep_h uct_ep, uint8_t am_id, const void *header,
                                unsigned header_length, const uct_iov_t *iov,
                                size_t iovcnt, unsigned flags,
                                uct_completion_t *comp);

static UCS_F_ALWAYS_INLINE ucs_status_t
uct_sg_ep_put_comp_add(uct_sg_ep_t *ep, uct_completion_t *comp, int wait_sn);

ucs_status_t uct_sg_ep_put_zcopy(uct_ep_h uct_ep, const uct_iov_t *iov,
                                 size_t iovcnt, uint64_t remote_addr,
                                 uct_rkey_t rkey, uct_completion_t *comp);

ucs_status_t uct_sg_ep_pending_add(uct_ep_h tl_ep, uct_pending_req_t *req,
                                   unsigned flags);

static void uct_sg_ep_pending_purge_cb(uct_pending_req_t *self, void *arg);

void uct_sg_ep_pending_purge(uct_ep_h tl_ep, uct_pending_purge_callback_t cb,
                             void *arg);

ucs_status_t uct_sg_ep_flush(uct_ep_h tl_ep, unsigned flags,
                             uct_completion_t *comp);

ucs_status_t
uct_sg_ep_check(uct_ep_h tl_ep, unsigned flags, uct_completion_t *comp);

ucs_status_t uct_sg_ep_create(const uct_ep_params_t *params,
                              uct_ep_h *ep_p);

void uct_sg_ep_destroy(uct_ep_h tl_ep);

ucs_status_t uct_sg_ep_get_address(uct_ep_h tl_ep, uct_ep_addr_t *ep_addr);

/**
 * Query for active network devices under /sys/class/net, as determined by
 * ucs_netif_is_active().
 */
ucs_status_t uct_sg_query_devices(uct_md_h md,
                                  uct_tl_device_resource_t **devices_p,
                                  unsigned *num_devices_p);

#endif