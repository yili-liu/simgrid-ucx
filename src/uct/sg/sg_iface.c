
#include "sg.h"

#include <ucs/async/async.h>
#include <ucs/sys/string.h>
#include <ucs/config/types.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <dirent.h>
#include <float.h>

#include <uct/base/uct_iface.h>

extern ucs_class_t UCS_CLASS_DECL_NAME(uct_sg_iface_t);
static UCS_CLASS_DEFINE_DELETE_FUNC(uct_sg_iface_t, uct_iface_t);

ucs_status_t uct_sg_iface_flush() 
{
    printf("in uct_sg_iface_flush\n");
    #warning
}

unsigned uct_sg_iface_progress(uct_iface_h tl_iface) 
{
    return 0;
}

ucs_status_t uct_sg_iface_event_fd_get() 
{
    printf("in uct_sg_iface_event_fd_get\n");
    #warning
}

ucs_status_t uct_sg_iface_query() 
{
    printf("in uct_sg_iface_query\n");
    #warning
}

ucs_status_t uct_sg_iface_get_address() 
{
    printf("in uct_sg_iface_get_address\n");
    #warning
}

ucs_status_t uct_sg_iface_get_device_address() 
{
    printf("in uct_sg_iface_get_device_address\n");
    #warning
}

static int uct_sg_iface_is_reachable(const uct_iface_h tl_iface,
                                     const uct_device_addr_t *dev_addr,
                                     const uct_iface_addr_t *iface_addr) 
{
    printf("in uct_sg_iface_is_reachable\n");
    return 1;
}

ucs_status_t uct_sg_query_devices(uct_md_h md,
                                  uct_tl_device_resource_t **devices_p,
                                  unsigned *num_devices_p) 
{
    printf("in uct_sg_query_devices\n");

    unsigned num_devices = 0; // TODO placeholder
    *num_devices_p = num_devices; 
    return UCS_OK;
}

static uct_iface_ops_t uct_sg_iface_ops = {
    .ep_am_short              = uct_sg_ep_am_short,
    .ep_am_short_iov          = uct_sg_ep_am_short_iov,
    .ep_am_bcopy              = uct_sg_ep_am_bcopy,
    .ep_am_zcopy              = uct_sg_ep_am_zcopy,
    .ep_put_zcopy             = uct_sg_ep_put_zcopy,
    .ep_pending_add           = uct_sg_ep_pending_add,
    .ep_pending_purge         = uct_sg_ep_pending_purge,
    .ep_flush                 = uct_sg_ep_flush,
    .ep_fence                 = uct_base_ep_fence,
    .ep_check                 = uct_sg_ep_check,
    .ep_create                = uct_sg_ep_create,
    .ep_destroy               = uct_sg_ep_destroy,
    .ep_get_address           = uct_sg_ep_get_address,
    .ep_connect_to_ep         = uct_base_ep_connect_to_ep,
    .iface_flush              = uct_sg_iface_flush,
    .iface_fence              = uct_base_iface_fence,
    .iface_progress_enable    = uct_base_iface_progress_enable,
    .iface_progress_disable   = uct_base_iface_progress_disable,
    .iface_progress           = uct_sg_iface_progress,
    .iface_event_fd_get       = uct_sg_iface_event_fd_get,
    .iface_event_arm          = ucs_empty_function_return_success,
    .iface_close              = UCS_CLASS_DELETE_FUNC_NAME(uct_sg_iface_t),
    .iface_query              = uct_sg_iface_query,
    .iface_get_address        = uct_sg_iface_get_address,
    .iface_get_device_address = uct_sg_iface_get_device_address,
    .iface_is_reachable       = uct_sg_iface_is_reachable
};

static uct_iface_internal_ops_t uct_sg_iface_internal_ops = {};

static UCS_CLASS_INIT_FUNC(uct_sg_iface_t, uct_md_h md, uct_worker_h worker,
                           const uct_iface_params_t *params,
                           const uct_iface_config_t *tl_config)
{
    printf("in uct_sg_iface_t init\n"); // s4ucx

    UCS_CLASS_CALL_SUPER_INIT(
            uct_base_iface_t, &uct_sg_iface_ops, &uct_sg_iface_internal_ops,
            md, worker, params,
            tl_config UCS_STATS_ARG(
                    (params->field_mask & UCT_IFACE_PARAM_FIELD_STATS_ROOT) ?
                            params->stats_root :
                            NULL) UCS_STATS_ARG(params->mode.device.dev_name));

    return UCS_OK;
}

static UCS_CLASS_CLEANUP_FUNC(uct_sg_iface_t)
{
    printf("in uct_sg_iface_t cleanup\n");
    ucs_status_t status;

    ucs_debug("sg_iface %p: destroying", self);
}

UCS_CLASS_DEFINE(uct_sg_iface_t, uct_base_iface_t);
static UCS_CLASS_DEFINE_NEW_FUNC(uct_sg_iface_t, uct_iface_t, uct_md_h,
                                 uct_worker_h, const uct_iface_params_t*,
                                 const uct_iface_config_t*);


ucs_status_t uct_sg_ep_am_short(uct_ep_h uct_ep, uint8_t am_id, uint64_t header,
                                const void *payload, unsigned length)
{
    printf("in uct_sg_ep_am_short\n");
    #warning
}

ucs_status_t uct_sg_ep_am_short_iov(uct_ep_h uct_ep, uint8_t am_id,
                                    const uct_iov_t *uct_iov, size_t uct_iov_cnt)
{
    printf("in uct_sg_ep_am_short_iov\n");
    #warning
}

ssize_t uct_sg_ep_am_bcopy(uct_ep_h uct_ep, uint8_t am_id,
                           uct_pack_callback_t pack_cb, void *arg,
                           unsigned flags)
{
    printf("in uct_sg_ep_am_bcopy\n");
    #warning
}

ucs_status_t uct_sg_ep_am_zcopy(uct_ep_h uct_ep, uint8_t am_id, const void *header,
                                unsigned header_length, const uct_iov_t *iov,
                                size_t iovcnt, unsigned flags,
                                uct_completion_t *comp)
{
    printf("in uct_sg_ep_am_zcopy\n");
    #warning
}

static UCS_F_ALWAYS_INLINE ucs_status_t
uct_sg_ep_put_comp_add(uct_sg_ep_t *ep, uct_completion_t *comp, int wait_sn)
{
    printf("in uct_sg_ep_put_comp_add\n");
    #warning
}

ucs_status_t uct_sg_ep_put_zcopy(uct_ep_h uct_ep, const uct_iov_t *iov,
                                 size_t iovcnt, uint64_t remote_addr,
                                 uct_rkey_t rkey, uct_completion_t *comp)
{
    printf("in uct_sg_ep_put_zcopy\n");
    #warning
}

ucs_status_t uct_sg_ep_pending_add(uct_ep_h tl_ep, uct_pending_req_t *req,
                                   unsigned flags)
{
    printf("in uct_sg_ep_pending_add\n");
    #warning
}

static void uct_sg_ep_pending_purge_cb(uct_pending_req_t *self, void *arg)
{
    printf("in uct_sg_ep_pending_purge_cb\n");
    #warning
}

void uct_sg_ep_pending_purge(uct_ep_h tl_ep, uct_pending_purge_callback_t cb,
                             void *arg)
{
    printf("in uct_sg_ep_pending_purge\n");
    #warning
}

ucs_status_t uct_sg_ep_flush(uct_ep_h tl_ep, unsigned flags,
                             uct_completion_t *comp)
{
    printf("in uct_sg_ep_flush\n");
    #warning
}

ucs_status_t
uct_sg_ep_check(uct_ep_h tl_ep, unsigned flags, uct_completion_t *comp)
{
    printf("in uct_sg_ep_check\n");
    #warning
}

void uct_sg_ep_destroy(uct_ep_h tl_ep) 
{
    printf("in uct_sg_ep_destroy\n");
    #warning
}

ucs_status_t uct_sg_ep_get_address(uct_ep_h tl_ep, uct_ep_addr_t *ep_addr) 
{
    printf("in uct_sg_ep_get_address\n");
    #warning
}

/**
 * Transport registration routines
 *
 * @param _component      Component to add the transport to
 * @param _name           Name of the transport (should be a token, not a string)
 * @param _query_devices  Function to query the list of available devices
 * @param _iface_class    Struct type defining the uct_iface class
 * @param _cfg_prefix     Prefix for configuration variables
 * @param _cfg_table      Transport configuration table
 * @param _cfg_struct     Struct type defining transport configuration
 */

UCT_TL_DEFINE_ENTRY(&uct_sg_component, sg, uct_sg_query_devices,
                    uct_sg_iface_t, UCT_SG_CONFIG_PREFIX,
                    uct_sg_iface_config_table, uct_sg_iface_config_t);

UCT_SINGLE_TL_INIT(&uct_sg_component, sg,,,)
